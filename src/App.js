import React, { useState, useEffect } from "react";
import CreateGlobalStyle from "./styles";
import NavBar from "./components/NavBar/index";
import Card from "./components/Card/index";
import TagList from "./components/TagList/index";
import axios from "axios";
import { Container, Row, Col, Button } from "react-bootstrap";
import { FaSadCry } from "react-icons/fa";

function App() {
  const [data, setData] = useState([]);

  const getData = async () => {
    let dados = await axios.get("http://localhost:8081/notas");
    return await Object.entries(dados.data.notas)
      .map(d => d[1])
      .reverse();
  };

  useEffect(() => {
    getData().then(dados => setData(dados));
  });

  const dataChanged = () => getData().then(dados => setData(dados));

  const filterData = filtro =>
    getData().then(dados =>
      setData(
        dados.filter(dado =>
          String(dado)
            .toLowerCase()
            .includes(filtro)
        )
      )
    );

  return (
    <>
      <CreateGlobalStyle />
      <NavBar dataChanged={dataChanged} filterData={filterData} />

      <Container fluid>
        <Row className="mainContainer">
          <Col xs={2}>
            {data.length != 0 ? <p>Tags</p> : null}
            {data.map(dado => (
              <TagList key={dado.id} text={dado.tag} />
            ))}
          </Col>
          <Col>
            {data.map(dado => (
              <Card
                dataChanged={dataChanged}
                key={dado._id}
                id={dado._id}
                title={dado.titulo}
                text={dado.texto}
                tag={dado.tag}
              />
            ))}
            {data.length == 0 ? (
              <h2>
                Sem notas <FaSadCry />
              </h2>
            ) : null}
          </Col>
        </Row>
        <Row>
          <Col></Col>
        </Row>
      </Container>
    </>
  );
}

export default App;
