import styled, { createGlobalStyle} from 'styled-components';

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        outline: 0;
        box-sizing: border-box;
    }

    .mainContainer {
        margin-top: 20px;
    }

    
    html, body, #root {
        height: 100%;
    }

   
`;