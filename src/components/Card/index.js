import React, { useState } from "react";
import { Card, Button } from "react-bootstrap";
import { FaEdit, FaTrashAlt } from "react-icons/fa";
import axios from "axios";
import EditPost from '../Posts/editPost';
import StyleDiv from "./styles";

export default props => {

  const [modal, setModal] = useState(false);  

  const abrirModal = () => setModal(true);

  const fecharModal = () => {
    setModal(false);
    props.dataChanged();
  };


  const deleteCard = () => {
    axios
      .get("http://localhost:8081/deletarNotas/" + props.id)
      .then(() => props.dataChanged());
  };


  return (
    <StyleDiv>
      <Card style={{ width: "18rem", float: "left" }}>
        <Card.Img variant="top" src={props.img} />
        <Card.Body>
          {modal == true && <EditPost id={props.id} titulo={props.title} texto={props.text} tag={props.tag} fecharModal={fecharModal} />}
          <Card.Title>{props.title}</Card.Title>
          <Card.Text>{props.text}</Card.Text>
          <Card.Text style={{ color: "grey", fontSize: "10px"}}>{props.tag}</Card.Text>
          <Button variant="light">
            <FaEdit onClick={abrirModal}/>
          </Button>
          <Button onClick={deleteCard} variant="light">
            <FaTrashAlt />
          </Button>
        </Card.Body>
      </Card>
    </StyleDiv>
  );
};
