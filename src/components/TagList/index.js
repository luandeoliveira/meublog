import React from "react";
import { Form, ListGroup } from "react-bootstrap";

export default props => {
  return (
    <>
      <ListGroup as="ul">
        <ListGroup.Item as="li"><a href="#">{props.text}</a></ListGroup.Item>        
      </ListGroup>
    </>
  );
};
