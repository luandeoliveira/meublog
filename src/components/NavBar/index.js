import React, { useState } from "react";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import AddPost from "../Posts/addPost";
import { FaSearch, FaPlusCircle } from "react-icons/fa";

export default props => {
  const [modal, setModal] = useState(false);
  const [search, setSearch] = useState("");

  const abrirModal = () => setModal(true);

  const fecharModal = () => {
    setModal(false);
    props.dataChanged();
  };
 
  const procurar = () => props.filterData(search)

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">          

            {modal == true && <AddPost fecharModal={fecharModal} />}

            <Nav.Link>
              <Button onClick={abrirModal}>
                <FaPlusCircle />
              </Button>
            </Nav.Link>
            
            
          </Nav>
          <Form inline>
            <FormControl
              value={search}
              onChange={e => setSearch(e.target.value)}
              type="text"
              placeholder=""
              className="mr-sm-2"
            />            
            <Button onClick={procurar} variant="dark">
              <FaSearch />
            </Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};
