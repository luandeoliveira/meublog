import React, { useState } from "react";
import { Form, Modal, Button } from "react-bootstrap";
import axios from "axios";

export default props => {
  const [titulo, setTitulo] = useState(props.titulo);
  const [texto, setTexto] = useState(props.texto);
  const [tag, setTag] = useState(props.tag);

  const editData = () => {
    const dados = {
      titulo: titulo,
      texto: texto,
      tag: tag
    };

    (async () => {
      await axios.post(`http://localhost:8081/editarNotas/${props.id}`, dados);
      await props.fecharModal();
    })();

    
  };

  return (
    <>
      <Modal show={true} onHide={props.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Adicionar Nota</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Label>Título</Form.Label>
            <Form.Control
              type="text"
              value={titulo}
              onChange={e => setTitulo(e.target.value)}
            />
            <Form.Group>
              <Form.Label>Conteúdo</Form.Label>
              <Form.Control
                value={texto}
                onChange={e => setTexto(e.target.value)}
                as="textarea"
                rows="5"
              />
              <Form.Label>Tag</Form.Label>
              <Form.Control
                value={tag}
                onChange={e => setTag(e.target.value)}
                type="text"
                rows="5"
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.fecharModal} variant="secondary">
            Cancelar
          </Button>
          <Button onClick={editData} variant="primary">
            Salvar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
