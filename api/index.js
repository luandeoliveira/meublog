const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const Notas = require("./bd");
const cors = require("cors");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.get("/notas", (req, res) => {
  Notas.find().then(notas => res.json({ notas: notas }));
});

app.get("/deletarNotas/:id", (req, res) => {
  Notas.deleteOne({ _id: req.params.id })
    .then(console.log("Sucesso"))
    .catch(erro => res.send(erro));
});

app.post("/editarNotas/:id", (req, res) => {
  Notas.findOneAndUpdate(
    { _id: req.params.id },
    {
      titulo: req.body.titulo,
      texto: req.body.texto,
      tag: req.body.tag
    }
  ).then(res.send("Sucesso"));
});

app.get("/buscarNotas/:busca", (req, res) => {
  let busca = req.params.busca.toLowerCase();
  Notas.find({ titulo: new RegExp(busca, 'i') }).then(result => {
    res.json({ result: result });
  });
});

app.post("/addNotas", (req, res) => {
  const nota = {
    titulo: req.body.titulo,
    texto: req.body.texto,
    tag: req.body.tag
  };

  const data = new Notas(nota);
  data.save();
});

app.listen(8081, function() {
  console.log("Servidor rodando!");
});
