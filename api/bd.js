const mongoose = require("mongoose");
const config = require("./config");

mongoose.connect(config, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

const Schema = mongoose.Schema;

const notasSchema = new Schema({  
    titulo: {type: String, required: true},  
    texto: String,  
    tag: String  
}, {collection: 'notas'});  

const Notas = mongoose.model('notas', notasSchema);  

module.exports = Notas; 

